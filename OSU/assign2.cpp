//Tool to find Admission status with GPA and SAT
//Tobi Fanibi
#include <iostream>

using namespace std;

int main()
{
    double gpa;
     int sat;
     cout<<"Enter GPA: ";
     cin>>gpa;

     //no more checks if GPA is above 4.0
     if (gpa>4.0)
         cout<<"Admit: ";
     else
     {
         //Declare these variables since they are used in every conditional now
         char highGPA=gpa<=3.99 and gpa>=3.5;
         char goodGPA=gpa<=3.49 and gpa>=3.0;


       cout<<"Enter SAT: ";
       cin>>sat;
       //if sat is above 2200 no need for more checks
       if (sat>=2200)
           cout<<"Admit ";
       //for all GPA less then 4.0 and sat less then 2200


       //parameters for Deny
       else if((gpa<3.0 and sat<2200) or (goodGPA and sat<1500) or (highGPA and sat<1100))
            cout<<"Deny: ";
       // parameters for Admit
       else if((goodGPA and sat>2000)or (highGPA and sat>1400) )
            cout<<"Admit: ";
       //parameters for Waitlist
       else if((goodGPA and 1500<=sat and 2000>=sat) or(highGPA and sat<=1400 and sat>=1100 ))
            cout<<"Waitlist ";

     }

    return 0;


}


