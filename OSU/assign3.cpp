#include <iostream>
#include <cmath>
using namespace std;

//Trying out a a few simple function goes. Maybe useful if multiple angles are needed in future.
double degreetoRad(double degree)
{
double convert=degree* (3.14159/180);
return convert;

}

double radtoDegree(double radian)
{
double convert=radian*(180/3.14159);
return convert;
}

int main()
{
    //Variables need for user to enter
    double sideA;
    double sideB;
    double degreeA;
    //When using abs to compare sideA to 15*sin(20 degrees) or 1500*sin(20 degrees) or 15000*sin(20 degrees) required delta seems to decrease with size?
    const double EPSILON = 1E-5;
    //Prompts for user
    cout << "Enter Side a" << endl;
    cin>>sideA;
    cout << "Enter Side b" << endl;
    cin>>sideB;
    cout << "Enter A:" << endl;
    cin>>degreeA;
    double a1=degreetoRad(degreeA);



    // Conditional within conditional for Acute solutions
    if(degreeA<90 and sideA<sideB)
        {
        if(sideA<(sideB*sin(a1)))
            {cout<<"No solution"<<endl;}
        else if(abs(sideA-(sideB*sin(a1)))<EPSILON)
            {
            double sinB=(sideB*sin(a1))/sideA;
            double radB=asin(sinB);
            double degreeB=radtoDegree(radB);
            double degreeC=180-degreeA-degreeB;
            cout<<"Angle B ="<<degreeB<<endl;
            cout<<"Angle C ="<<degreeC<<endl;}

        else if(sideA<sideB and sideA>(sideB*sin(a1)))
           {double sinB=(sideB*sin(a1))/sideA;
            double radB=asin(sinB);
            double degreeB1=radtoDegree(radB);
            double degreeB2=180-degreeB1;
            double degreeC1=180-degreeA-degreeB1;
            double degreeC2=180-degreeA-degreeB2;
            cout<<"First Solution ="<<endl;
            cout<<"Angle B ="<<degreeB1<<endl;
            cout<<"Angle C ="<<degreeC1<<endl;
            cout<<"Secound Solution ="<<endl;
            cout<<"Angle B ="<<degreeB2<<endl;
            cout<<"Angle C ="<<degreeC2<<endl;
            }



        //Pathway leads to only one solution
        if(degreeA<90 and sideA>sideB)
           {
            double sinB=(sideB*sin(a1))/sideA;
            double radB=asin(sinB);
            double degreeB=radtoDegree(radB);
            double degreeC=180-degreeA-degreeB;
            cout<<"Angle B ="<<degreeB<<endl;
            cout<<"Angle C ="<<degreeC<<endl;}
         }

    //Pathway leads to only one solution
    if(degreeA<90 and sideA>sideB)
       {
        double sinB=(15*sin(a1))/sideA;
        double radB=asin(sinB);
        double degreeB=radtoDegree(radB);
        double degreeC=180-degreeA-degreeB;
        cout<<"Angle B ="<<degreeB<<endl;
        cout<<"Angle C ="<<degreeC<<endl;}




    //Includes two conditionals for obtuse solutions
    if(degreeA>90)
    {
       if(sideA<=sideB)
       {cout<<"No solution"<<endl;}
       if(sideA>sideB)
       {
       double sinB=(15*sin(a1))/sideA;
       double radB=asin(sinB);
       double degreeB=radtoDegree(radB);
       double degreeC=180-degreeA-degreeB;
       cout<<"Angle B ="<<degreeB<<endl;
       cout<<"Angle C ="<<degreeC<<endl;


       }


    }


    }


