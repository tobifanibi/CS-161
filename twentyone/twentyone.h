#include<string>
using namespace std;
#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#ifndef TWENTYONE_H
#define TWENTYONE_H


/**
 * @brief cardtotals  find the total for the cards including the values from the intitial two cards
 * @param player the specific player we are calculating for
 * @param totalplayer the total of value of the cards for the player, so we can find when it reaches 21 or the current total for the user
 * @param playerMoney How much money the player has, if it is zero they are skipped.
 */
void cardtotals(string player,int &totalplayer,int playerMoney);
/**
 * @brief cardtotalsdealer dealer gets to draw one last card, this adds that card value to the total dealer card. Card is random
 * @param totaldealer where we store the value of all card for the dealer
 * @return return total dealer to main
 */
int cardtotalsdealer(int totaldealer);
/**
 * @brief cardinitital we store the value for the two first cards drawn by the player
 * @param playerMoney if the player does not have enough money, then no option to draw cards
 * @param player the specific player we are currently working with
 * @return return the total for the first two cards combined
 */
int cardinitital(int playerMoney,string player);
/**
 * @brief winlose Find whether the user has one or lost based on the dealer total, and their total
 * @param totalplayer the total value of the cards for the specific player
 * @param totaldealer the total value of the cards for the dealer
 * @param playerMoney how much money the player has
 * @param bet how much the bet they are betting, note money calc makes sure we don't go negative
 * @param player the current player we are looking at.
 */

void winlose(int totalplayer,int totaldealer,int&playerMoney,int&bet,string player);
/**
 * @brief moneycalc find the amount the user is betting for the round
 * @param playerMoney the amount of money the player has
 * @param bet how much money they are betting
 * @param player the player we are looking at
 */
void moneycalc(int &playerMoney, int &bet,string player);
/**
 * @brief inputter make sure only valid inputs are taken in when we prompt the user
 * @param userinput the variable where the user input is put in.
 */
void inputter(int &userinput);


#endif // TWENTYONE_H
