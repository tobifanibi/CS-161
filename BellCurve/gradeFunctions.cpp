#include <string>
#include <iostream>
#include <math.h>
using namespace std;
void getArrayData(int scores[], int size)
{
   for(int i=0;i<size;i++)
   {
      //cout<<i+1<< " ";
      cin>>scores[i];

   }
}

double average(const int scores[], int size)
{
    int total=0;
    for(int i=0;i<size;i++)
    {
        total=total+scores[i];
    }

    double avg=total/size;
    return avg;




}

double standardDeviation(const int scores[], int size, double mean)
{

   double difference[10];
   for(int i=0;i<size;i++)
   {
       difference[i]=scores[i]-mean;
       difference[i]=difference[i]*difference[i];
  }

   int total=0;
   for(int i=0;i<size;i++)
   {
       total=total+difference[i];

   }
     double avg=total/size;
     double stdev=sqrt(avg);
     return stdev;



}

char getLetterGrade(int score, double mean, double stdDev)
{
    int difference=score-mean;
    double numberOfStdDev=difference/stdDev;

    if(numberOfStdDev<-1)
       {
        char grade='F';
        return grade;
        }
    else if(numberOfStdDev>=-1 and numberOfStdDev<=-.5 )
    {
     char grade='D';
     return grade;
     }

    else if(numberOfStdDev>-.5 and numberOfStdDev<.5 )
    {
     char grade='C';
     return grade;
     }

    else if(numberOfStdDev>.5 and numberOfStdDev<1 )
    {
     char grade='B';
     return grade;
     }

    else if( numberOfStdDev>1 )
    {
     char grade='A';
     return grade;
     }




}

void printstudent(int size)
{
    cout<<"Student: ";
    for(int i=0;i<size;i++)
    {
       cout<<i+1<<" ";

    }
}

