//Calculate Grades based on the bell curve, using standard deviations
#include <string>
#include <iostream>
#include "gradeFunctions.h"

using namespace std;

int main()
{
int scores[]={70,85,75,65,82,96,58,93,86,90};
//int scores[10];
//cout<<"Enter 10 Grades"<<endl;

//Functions to gather values needed, and cout to send to user
//getArrayData(scores, 10);
int gradeavg = average(scores, 10);
cout<<"The average is: "<<gradeavg<<endl;
double standdev=standardDeviation(scores,10,gradeavg);
cout<<"The average is: "<<standdev<<endl;

//Print all the student numbers, and then show the final grade
printstudent(10);
cout<<endl;
cout<<"Grade:   ";
for(int i=0;i<10;i++)
{
    char grade=getLetterGrade(scores[i], gradeavg, standdev);
    cout<<grade<<" ";

}


}







